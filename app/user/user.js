'use strict';

angular.module('pagarme.user', [
  'ngAnimate',
  'ngCookies',
  'ui.router',
  'ngSanitize',
  'ngResource',
  'ngMaterial'
])

.config(['$stateProvider', function($stateProvider) {

  $stateProvider
    .state('user', {
      url: '/user',
      views: {
        'content': {
          templateUrl: '/app/user/index.html',
          controller: 'UserController'
        }
      }
    });

}])

.controller('UserController', ['$scope', 'User', '$state', '$stateParams', '$mdToast', function($scope, User, $state, $stateParams, $mdToast) {
  $scope.getUserRepositories = function(user){
    User.repositories({username: user}).$promise
      .then(function(repos){
        $stateParams.repos = repos;
        $stateParams.user = user;
        $state.go('repos', { repos: repos, user: user });
      })
      .catch(function(err){
        $mdToast.show(
          $mdToast.simple()
            .content(err.statusText)
            .position('right top')
            .hideDelay(3000)
        );
      });
  }
}])

.factory('User', function ($resource) {
  return $resource(
    'https://api.github.com/users/:username/repos',
    {},
    {
      'repositories': {
        method: 'GET',
        isArray: true,
        params: {
          client_secret: '54ed193a40548060a384b38f84ffc379975c6131',
          client_id: '69593d3344135c9b03ac',
          per_page: 5000
        }
      }
    }
  );
});
