'use strict';
describe('User', function() {
  describe('UserFactory', function() {
    var User;
    var $httpBackend;
    beforeEach(module('pagarme.user'));

    beforeEach(inject(function(_User_, _$httpBackend_) {
      User = _User_;
      $httpBackend = _$httpBackend_;
    }));

    it('should return repos list from user', function() {
      var result;

      $httpBackend.whenGET('https://api.github.com/users/:username/repos').respond([1, 2, 3]);
      sampleService.getData().then(function(response) {
        result = response.data;
      });
      $httpBackend.flush();

      expect(result).toEqual([1, 2, 3]);
    });
  });

  describe('UserController', function() {
    var $rootScope,
        ctrl, scope;

    beforeEach(module('pagarme.user'));

    beforeEach(inject(function(_$rootScope_, _$controller_) {

        $rootScope = _$rootScope_;

        // create a brand new scope
        scope = $rootScope.$new();

        ctrl = _$controller_('UserController', {
            $scope: scope
        });
    }));
  });
});