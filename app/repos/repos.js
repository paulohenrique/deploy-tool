'use strict';

angular.module('pagarme.repos', [
  'ngAnimate',
  'ngCookies',
  'ui.router',
  'ngSanitize',
  'ngResource',
  'ngMaterial'
])

.config(['$stateProvider', function($stateProvider) {

  $stateProvider
    .state('repos', {
      url: '/repos',
      params: {
        repos: null,
        user: null
      },
      views: {
        'content': {
          templateUrl: '/app/repos/index.html',
          controller: 'ReposController'
        }
      }
    })
    .state('repo', {
      url: '/repos/:name',
      params: {
        repo: null,
        user: null
      },
      views: {
        'content': {
          templateUrl: '/app/repos/repo.html',
          controller: 'ReposController'
        }
      }
    });

}])

.controller('ReposController', ['$scope', '$stateParams','$state', '$timeout', 'Repo', '$mdToast', function($scope, $stateParams, $state, $timeout, Repo, $mdToast) {
  $scope.user = $stateParams.user;
  $scope.repos = $stateParams.repos;
  $scope.displayLoading = false;
  $scope.disabled = true;
  $scope.repo = $stateParams.repo;

  $scope.selectRepo = function(repo){
    $state.go('repo', {name: repo.name, user: $scope.user, repo: repo})
  };

  $scope.syncRepo = function(index, repo){
    $scope.selected = repo;
    $scope.displayLoading = index;

    //faking a process
    $timeout(function() {
      $scope.disabled = index;
      $scope.displayLoading = false;
      $scope.getBranches(repo.owner.login, repo.name);
    }, 10000);
  };

  $scope.getBranches = function(owner, repo){
    Repo.branches({owner: owner, repo: repo}).$promise
      .then(function(branches){
        $scope.branches = branches;
        $scope.pullrequests = null;
      });
  };

  $scope.getPullRequests = function(owner, repo){
    Repo.pullrequests({owner: owner, repo: repo}).$promise
      .then(function(pullrequests){
        $scope.pullrequests = pullrequests;
        $scope.branches = null;
      });
  };

  $scope.deploy - function(){
    $mdToast.show(
      $mdToast.simple()
        .content('Sorry! We are working on it yet!')
        .position('right top')
        .hideDelay(8000)
    );
  }
}])

.factory('Repo', function ($resource) {
  return $resource(
    'https://api.github.com/repos/:owner/:repo/branches',
    {},
    {
      'branches': {
        method: 'GET',
        isArray: true,
        params: {
          client_secret: '54ed193a40548060a384b38f84ffc379975c6131',
          client_id: '69593d3344135c9b03ac',
          per_page: 5000
        }
      },
      'pullrequests': {
        url: 'https://api.github.com/repos/:owner/:repo/pulls',
        method: 'GET',
        isArray: true,
        params: {
          client_secret: '54ed193a40548060a384b38f84ffc379975c6131',
          client_id: '69593d3344135c9b03ac',
          per_page: 5000
        }
      }
    }
  );
});
