'use strict';

angular.module('pagarme', [
  'ui.router',
  'pagarme.user',
  'pagarme.repos'
]).
config(function($urlRouterProvider){
  $urlRouterProvider.otherwise('/user');
});
