var express = require('express');
var app = express();


// Static routes first
app.use('/bower_components', express.static(__dirname + '/bower_components'));
app.use('/app', express.static(__dirname + '/app'));
app.use('/public', express.static(__dirname + '/public'));


app.all('/*', function(req, res) {
  res.sendFile(__dirname + '/app/index.html');
});

var portNumber = process.env.PORT || 3000;

app.listen(portNumber);

console.log('Booya! Server now running on port', portNumber);
