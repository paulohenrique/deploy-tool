# Deploy Tool #

Deploy tool allow developers to deploy branches and pull requests easy with a click.
### How do I access ? ##
* [Click Here](https://pagarmedeploytool.herokuapp.com)
### How do I get set up? ###

* `git clone git@bitbucket.org:paulohenrique/deploy-tool.git`
* Needs Node.js
* How to run: `npm install && npm start`
* How to run tests: `karma start`

### Who do I talk to? ###

* Paulo Pires <ph.pires@icloud.com>